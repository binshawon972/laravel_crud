<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductRequest;
use App\Models\Product;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class ProductsController extends Controller
{
    public function index()
    {
        $products =  Product::orderBy('id','desc')->get();
        return view('backend/products/index', compact('products'));
    }
    public function create()
    {

        return view('backend/products/create');
    }
    public function store(ProductRequest $request)
    {
        try {
            // $request->validate();
            Product::create($request->all());
            return redirect()->route('dashboard.products')->with('message','Product created!');
        } catch (QueryException $q) {
            return redirect()->back()->withInput()->withErrors($q->getMessage());
        }
    }
    public function show($id)
    {
        $product = Product::findOrFail($id);
        // dd($product);
        return view('backend/products/view',compact('product'));
    }
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        // dd($product);
        return view('backend/products/edit',compact('product'));
    }
    public function update(ProductRequest $request ,$id)
    {
        try {
            
            Product::findOrFail($id)->update($request->all());
            return redirect()->route('dashboard.products')->with('message','Product details updated!');
        } catch (QueryException $q) {
            return redirect()->back()->withInput()->withErrors($q->getMessage());
        }
    }
    public function destroy($id)
    {
        Product::findOrFail($id)->delete();
        // dd($product);
        return redirect()->route('dashboard.products')->with('message','Product deleted!');
    }
}
