<?php

namespace App\Http\Controllers;

use App\Http\Requests\CategoryRequest;
use App\Models\Category;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    public function index()
    {
        $categories =  Category::orderBy('id','desc')->get();
        return view('backend/categories/index', compact('categories'));
    }
    public function create()
    {
        return view('backend/categories/create');
    }
    public function store(CategoryRequest $request)
    {
        try {
            $request->validate(
                [
                    'title' => 'required|max:30|unique:categories,title',
                    'description' => 'required|min:20|max:200'   
                ]
            );
            Category::create($request->all());
            return redirect()->route('dashboard.categories');
        } catch (QueryException $q) {
            return redirect()->back()->withInput()->withErrors($q->getMessage());
        }
    }
    public function show($id)
    {
        $category = Category::findOrFail($id);
        // dd($category);
        return view('backend/categories/view',compact('category'));
    }
    public function edit($id)
    {
        $category = Category::findOrFail($id);
        // dd($category);
        return view('backend/categories/edit',compact('category'));
    }
    public function update(CategoryRequest $request ,$id)
    {
        try {
            
            Category::findOrFail($id)->update($request->all());
            return redirect()->route('dashboard.categories')->with('message','Category details updated!');
        } catch (QueryException $q) {
            return redirect()->back()->withInput()->withErrors($q->getMessage());
        }
    }
    public function destroy($id)
    {
        Category::findOrFail($id)->delete();
        // dd($category);
        return redirect()->route('dashboard.categories')->with('message','Category deleted!');
    }
}
